import "reflect-metadata";
import {expect} from "chai";

import {
	State,
	STATE_MACHINE_STATE_META_KEY,
	Transitable,
	STATE_MACHINE_TRANSITION_META_KEY,
	TFSM,
	StateMachineClassAdapter,
} from "../src/AOP";
import {StateMachineError} from "../src/Error";

enum STATES {
	SOLID = "SOLID",
	LIQUID = "LIQUID",
	GAS = "GAS",
	PLASMA = "PLASMA",
}

enum TRANSITIONS {
	MELT = "MELT",
	VAPORIZE = "VAPORIZE",
	CONDENSE = "CONDENSE",
	FREEZE = "FREEZE",
}

// const log = (msg: string) => (from: any, to: any) => {
// 	console.log(msg, from.name + " --> " + to.name);
// };
const noop = () => {
};

type temperature = number;
const qwe = Symbol("qwe");

@TFSM(STATES.LIQUID)
class TestStateMachine extends StateMachineClassAdapter<STATES, TRANSITIONS, temperature> {
	public deg = "-20 deg";

	@State<STATES, TRANSITIONS, temperature>(
		STATES.SOLID,
		{
			before: noop
		}
	)
	public [STATES.SOLID]: Partial<TestStateMachine> = {
		deg: "-25 C"
	};

	@Transitable<STATES, TRANSITIONS, temperature>(
		TRANSITIONS.VAPORIZE,
		STATES.LIQUID,
		STATES.GAS, {before: [noop]})
	@Transitable<STATES, TRANSITIONS, temperature>(TRANSITIONS.FREEZE, STATES.LIQUID, STATES.SOLID, {before: [noop]})
	@State<STATES, TRANSITIONS, temperature>(STATES.LIQUID)
	public [STATES.LIQUID]: Partial<TestStateMachine> = {
		deg: "+5 C"
	};

	// @Transitable(
	// 	TRANSITIONS.CONDENSE,
	// 	STATES.GAS,
	// 	STATES.LIQUID)
	@Transitable<STATES, TRANSITIONS, temperature>(
		TRANSITIONS.CONDENSE,
		STATES.GAS,
		STATES.LIQUID,
		{before: [noop, noop]})
	@State(STATES.GAS)
	@State(STATES.PLASMA)
	public [STATES.GAS]: Partial<TestStateMachine> = {
		deg: "+105 C"
	};

	public asd = "asd";
	public [qwe] = "Symbol = qwe";

	// @StateMachine.Transitable(STATES.GAS, [STATES.LIQUID, STATES.PLASMA])
	// public [STATES.GAS] = {
	// 	deg: "+105 C"
	// };
}

describe("State Machine Metadata", () => {
	let TestSM: TestStateMachine;

	beforeEach("Setup class-based State Machine", () => {
		TestSM = new TestStateMachine();
		Reflect.deleteMetadata(STATE_MACHINE_STATE_META_KEY, TestSM, STATES.LIQUID);
		Reflect.deleteMetadata(STATE_MACHINE_STATE_META_KEY, TestSM, STATES.GAS);
		Reflect.deleteMetadata(STATE_MACHINE_TRANSITION_META_KEY, TestSM, STATES.GAS);
		Reflect.deleteMetadata(STATE_MACHINE_TRANSITION_META_KEY, TestSM, STATES.GAS);
	});

	it("enshure client class instance", () => {
		expect(TestSM).an.instanceof(TestStateMachine);
		expect(TestSM).an.instanceof(StateMachineClassAdapter);
	});

	it("reflect state metadata", () => {
		expect(Reflect.getMetadata(STATE_MACHINE_STATE_META_KEY, TestSM, STATES.LIQUID))
			.to.be.deep.equal([{
			name: STATES.LIQUID,
			data: {
				deg: "+5 C"
			},
			before: [],
			after: [],
		}]);
	});

	it("reflect several state metadata", () => {
		expect(Reflect.getMetadata(STATE_MACHINE_STATE_META_KEY, TestSM, STATES.GAS))
			.to.be.deep.equal([{
			name: STATES.GAS,
			data: {
				deg: "+105 C"
			},
			before: [],
			after: [],
		}, {
			name: STATES.PLASMA,
			data: {
				deg: "+105 C"
			},
			before: [],
			after: [],
		}]);
	});

	it("reflect transition metadata", () => {
		expect(Reflect.getMetadata(STATE_MACHINE_TRANSITION_META_KEY, TestSM, STATES.GAS))
			.to.be.deep.equal([{
			name: TRANSITIONS.CONDENSE,
			from: STATES.GAS,
			to: STATES.LIQUID,
			before: [noop, noop],
			after: [],
		}]);
	});

	it("reflect several transition metadata", async () => {
		expect(Reflect.getMetadata(STATE_MACHINE_TRANSITION_META_KEY, TestSM, STATES.LIQUID))
			.to.be.deep.equal([{
			name: TRANSITIONS.VAPORIZE,
			from: STATES.LIQUID,
			to: STATES.GAS,
			before: [noop],
			after: [],
		}, {
			name: TRANSITIONS.FREEZE,
			from: STATES.LIQUID,
			to: STATES.SOLID,
			before: [noop],
			after: [],
		}]);
	});

	describe("State Machine Class Adapter", () => {
		it("should return right state", () => {
			expect(TestSM.state)
				.to.be.equal(STATES.LIQUID);
		});

		it("should return right available states", () => {
			expect(TestSM.states)
				.to.have.members([STATES.GAS, STATES.SOLID]);
		});

		it("should return right all states", () => {
			expect(TestSM.allStates)
				.to.have.members([STATES.SOLID, STATES.LIQUID, STATES.PLASMA, STATES.GAS]);
		});

		it("should return right available transitions", () => {
			expect(TestSM.transitions)
				.to.have.members([TRANSITIONS.VAPORIZE, TRANSITIONS.FREEZE]);
		});

		it("should return right all transitions", () => {
			expect(TestSM.allTransitions)
				.to.have.members([TRANSITIONS.VAPORIZE, TRANSITIONS.FREEZE, TRANSITIONS.CONDENSE]);
		});

		it(`should return right "isPending" flag`, () => {
			expect(TestSM.isPending)
				.to.be.equal(false);
			TestSM.transitTo(STATES.SOLID);
			expect(TestSM.isPending)
				.to.be.equal(true);
		});

		it(`right "is" predicate `, async () => {
			expect(TestSM.is(STATES.PLASMA)).to.be.equal(false);
			expect(TestSM.is(() => STATES.LIQUID)).to.be.equal(true);
			expect(TestSM.is(await Promise.resolve(STATES.LIQUID))).to.be.equal(true);
			expect(TestSM.is(await Promise.resolve(STATES.LIQUID))).to.be.equal(true);
		});

		it(`right "canTransitTo" predicate`, async () => {
			expect(TestSM.canTransitTo(STATES.PLASMA)).to.be.equal(false);
			expect(TestSM.canTransitTo(() => STATES.LIQUID)).to.be.equal(false);
			expect(await TestSM.canTransitTo(Promise.resolve(STATES.SOLID))).to.be.equal(true);
			expect(TestSM.canTransitTo(STATES.GAS)).to.be.equal(true);
			expect(TestSM.canTransitTo(() => STATES.GAS)).to.be.equal(true);
			expect(await TestSM.canTransitTo(Promise.resolve(STATES.GAS))).to.be.equal(true);
		});

		it("should not pass 'canTransitTo' predicate", async () => {
			expect(TestSM.canTransitTo(STATES.GAS)).is.true;
			expect(TestSM.canTransitTo(() => STATES.GAS)).is.true;
			expect(await TestSM.canTransitTo(Promise.resolve(STATES.GAS))).is.true;
		});

		it("should pass 'canDoTransition' predicate", async () => {
			expect(TestSM.canDoTransition(TRANSITIONS.MELT)).is.false;
			expect(TestSM.canDoTransition(() => TRANSITIONS.MELT)).is.false;
			expect(await TestSM.canDoTransition(Promise.resolve(TRANSITIONS.MELT))).is.false;
		});

		it("should set right data", () => {
			expect(TestSM.deg).to.be.deep.equal("+5 C");
		});

		describe("State Machine Class Adapter: Throw", () => {
			it(`"doTransition" throw StateMachineError#PENDING_STATE`, async () => {
				await TestSM.doTransition(TRANSITIONS.VAPORIZE);
				expect(TestSM.isPending).is.false;
				TestSM.doTransition(TRANSITIONS.CONDENSE);
				expect(TestSM.isPending).is.true;

				try {
					await TestSM.doTransition(TRANSITIONS.VAPORIZE);
				} catch (error) {
					expect(error)
						.to.be.instanceOf(StateMachineError)
						.and.have.property("code", StateMachineError.ERROR_CODE.PENDING_STATE);
				}
			});

			it(`"transitTo" throw StateMachineError#PENDING_STATE`, async () => {
				await TestSM.transitTo(STATES.GAS);
				expect(TestSM.isPending).is.false;
				TestSM.transitTo(STATES.LIQUID);
				expect(TestSM.isPending).is.true;

				try {
					await TestSM.transitTo(STATES.GAS);
				} catch (error) {
					expect(error)
						.to.be.instanceOf(StateMachineError)
						.and.have.property("code", StateMachineError.ERROR_CODE.PENDING_STATE);
				}
			});

			it(`"is" throw StateMachineError#PENDING_STATE`, async () => {
				const testPromise = new Promise<STATES>(resolve => {
					setTimeout(() => {
						resolve(STATES.LIQUID)
					}, 0);
				});
				await TestSM.is(testPromise);
				TestSM.is(() => STATES.LIQUID);
				TestSM.is(STATES.LIQUID);
				expect(TestSM.isPending).is.false;
				TestSM.is(testPromise);
				expect(TestSM.isPending).is.true;

				try {
					await TestSM.is(Promise.resolve(STATES.SOLID));
				} catch (error) {
					expect(error)
						.to.be.instanceOf(StateMachineError)
						.and.have.property("code", StateMachineError.ERROR_CODE.PENDING_STATE);
				}
			});

			it(`"canTransitTo" throw StateMachineError#PENDING_STATE`, async () => {
				const testPromise = new Promise<STATES>(resolve => {
					setTimeout(() => {
						resolve(STATES.LIQUID)
					}, 0);
				});
				await TestSM.canTransitTo(testPromise);
				TestSM.canTransitTo(() => STATES.LIQUID);
				TestSM.canTransitTo(STATES.LIQUID);
				expect(TestSM.isPending).is.false;
				TestSM.canTransitTo(testPromise);
				expect(TestSM.isPending).is.true;

				try {
					await TestSM.canTransitTo(Promise.resolve(STATES.SOLID));
				} catch (error) {
					expect(error)
						.to.be.instanceOf(StateMachineError)
						.and.have.property("code", StateMachineError.ERROR_CODE.PENDING_STATE);
				}
			});

			it(`"canDoTransition" throw StateMachineError#PENDING_STATE`, async () => {
				const testPromise = new Promise<TRANSITIONS>(resolve => {
					setTimeout(() => {
						resolve(TRANSITIONS.CONDENSE)
					}, 0);
				});
				await TestSM.canDoTransition(testPromise);
				TestSM.canDoTransition(() => TRANSITIONS.CONDENSE);
				TestSM.canDoTransition(TRANSITIONS.CONDENSE);
				expect(TestSM.isPending).is.false;
				TestSM.canDoTransition(testPromise);
				expect(TestSM.isPending).is.true;

				try {
					await TestSM.canDoTransition(Promise.resolve(TRANSITIONS.CONDENSE));
				} catch (error) {
					expect(error)
						.to.be.instanceOf(StateMachineError)
						.and.have.property("code", StateMachineError.ERROR_CODE.PENDING_STATE);
				}
			});
		});
	});
});
