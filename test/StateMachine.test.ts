import { expect } from "chai";

import { StateMachine } from "../src/StateMachine";
import { StateMachineError } from "../src/Error";
import { IObject, IState, ITransition } from "../src/types";

enum STATES {
	SOLID = "SOLID",
	LIQUID = "LIQUID",
	GAS = "GAS",
	PLASMA = "PLASMA"
}

enum TRANSITIONS {
	MELT = "MELT",
	VAPORIZE = "VAPORIZE",
	CONDENSE = "CONDENSE",
	FREEZE = "FREEZE",
	SUBLIMATION = "SUBLIMATION",
	DESUBLIMATION = "DESUBLIMATION",
	RECOMBINATION = "RECOMBINATION",
	IONIZATION = "IONIZATION"
}

type temperature = number;

let TestSM: StateMachine<STATES, TRANSITIONS, temperature>;
let transitions: ITransition<STATES, TRANSITIONS, temperature>[];
let states: IState<STATES, TRANSITIONS, temperature>[];

describe("State Machine", () => {
	beforeEach("setup State Machine", () => {
		transitions = [
			{
				name: TRANSITIONS.MELT,
				from: STATES.SOLID,
				to: STATES.LIQUID
			},
			{
				name: TRANSITIONS.FREEZE,
				from: STATES.LIQUID,
				to: STATES.SOLID
			},
			{
				name: TRANSITIONS.VAPORIZE,
				from: STATES.LIQUID,
				to: STATES.GAS
			},
			{
				name: TRANSITIONS.CONDENSE,
				from: STATES.GAS,
				to: STATES.LIQUID,
				before: [
					() => {
						return new Promise(resolve => {
							setTimeout(() => {
								resolve(true);
							}, 5);
						});
					}
				]
			}
		];

		states = [
			{
				name: STATES.SOLID,
				data: -100
			},
			{
				name: STATES.LIQUID,
				data: 50
			},
			{
				name: STATES.GAS,
				data: 200
			}
		];
		const increaseEntropy = (transport: IObject) => {
			transport["entropy"] =
				transport["entropy"] === undefined
					? 0
					: transport["entropy"] + 1;
		};

		TestSM = new StateMachine<STATES, TRANSITIONS, temperature>(
			STATES.SOLID,
			{
				states
			},
			{
				transitions,
				after: increaseEntropy
			}
		);
	});

	it("should exist", () => {
		expect(TestSM).is.exist;
	});

	it("should be initialized with right state", () => {
		expect(TestSM.state).to.equal(STATES.SOLID);
	});

	it(`should pass "is" predicate`, async () => {
		expect(await TestSM.is(Promise.resolve(STATES.SOLID))).to.be.equal(
			true
		);
		expect(TestSM.is(STATES.SOLID)).to.be.equal(true);
		expect(TestSM.is(() => STATES.SOLID)).to.be.equal(true);
	});

	it("should not pass 'is' predicate", async () => {
		expect(await TestSM.is(STATES.LIQUID)).is.false;
		expect(await TestSM.is(() => STATES.LIQUID)).is.false;
	});

	it("should pass 'canTransitTo' predicate", async () => {
		expect(TestSM.canTransitTo(STATES.LIQUID)).is.true;
		expect(TestSM.canTransitTo(() => STATES.LIQUID)).is.true;
		expect(await TestSM.canTransitTo(Promise.resolve(STATES.LIQUID))).is
			.true;
	});

	it("should not pass 'canTransitTo' predicate", async () => {
		expect(TestSM.canTransitTo(STATES.GAS)).is.false;
		expect(TestSM.canTransitTo(() => STATES.GAS)).is.false;
		expect(await TestSM.canTransitTo(Promise.resolve(STATES.GAS))).is.false;
	});

	it("should pass 'canDoTransition' predicate", async () => {
		expect(TestSM.canDoTransition(TRANSITIONS.MELT)).is.true;
		expect(TestSM.canDoTransition(() => TRANSITIONS.MELT)).is.true;
		expect(await TestSM.canDoTransition(Promise.resolve(TRANSITIONS.MELT)))
			.is.true;
	});

	it("should not pass 'canDoTransition' predicate", async () => {
		expect(TestSM.canDoTransition(TRANSITIONS.CONDENSE)).is.false;
		expect(TestSM.canDoTransition(() => TRANSITIONS.CONDENSE)).is.false;
		expect(
			await TestSM.canDoTransition(Promise.resolve(TRANSITIONS.CONDENSE))
		).is.false;
	});

	it(`should transit to ${STATES.LIQUID}`, async () => {
		expect((await TestSM.transitTo(STATES.LIQUID)).state).to.equal(
			STATES.LIQUID
		);
	});

	it(`should transit to () => ${STATES.LIQUID}`, async () => {
		expect((await TestSM.transitTo(() => STATES.LIQUID)).state).to.equal(
			STATES.LIQUID
		);
	});

	it("should return right state after several transitions", async () => {
		expect((await TestSM.transitTo(STATES.LIQUID)).state).is.equal(
			STATES.LIQUID
		);
		expect((await TestSM.transitTo(STATES.GAS)).state).is.equal(STATES.GAS);
	});

	it("should return all possible states", () => {
		expect(TestSM.allStates).to.deep.equal(states.map(state => state.name));
	});

	it("should return all possible transitions", () => {
		expect(TestSM.allTransitions).to.deep.equal(
			transitions.map(transition => transition.name)
		);
	});

	it("should return list of transitions that are allowed from the current state", () => {
		expect(TestSM.transitions).to.deep.equal([transitions[0].name]);
	});

	it("should return right pending state", async () => {
		await TestSM.transitTo(STATES.LIQUID);
		expect(TestSM.isPending).is.false;
	});

	it("should return right pending state", async () => {
		await TestSM.transitTo(STATES.LIQUID);
		expect(TestSM.isPending).is.false;
		TestSM.transitTo(STATES.GAS);
		expect(TestSM.isPending).is.true;
	});

	it("check transport state", async () => {
		await TestSM.transitTo(STATES.LIQUID);
		expect(TestSM.transport.entropy).to.be.equal(0);
		await TestSM.transitTo(STATES.GAS);
		expect(TestSM.transport.entropy).to.be.equal(1);
		await TestSM.transitTo(STATES.LIQUID);
		expect(TestSM.transport.entropy).to.be.equal(2);
		await TestSM.transitTo(STATES.SOLID);
		expect(TestSM.transport.entropy).to.be.equal(3);
		await TestSM.transitTo(STATES.LIQUID);
		expect(TestSM.transport.entropy).to.be.equal(4);
	});

	describe("State Machine: Throws", () => {
		describe(`"constructor" throw StateMachineError`, () => {
			it(`"constructor" throw StateMachineError#ABSENT_STATE`, () => {
				try {
					new StateMachine(
						STATES.PLASMA, // ABSENT_STATE
						{ states: [] },
						{ transitions: [] }
					);
				} catch (error) {
					expect(error)
						.to.be.instanceof(StateMachineError)
						.and.have.property(
							"code",
							StateMachineError.ERROR_CODE.ABSENT_STATE
						);
				}
			});

			it(`"constructor" throw StateMachineError#DUPLICATED_STATE`, () => {
				try {
					new StateMachine(
						STATES.SOLID,
						{
							states: [
								{ name: STATES.SOLID, data: {} },
								{ name: STATES.SOLID, data: {} }
							]
						},
						{ transitions: [] }
					);
				} catch (error) {
					expect(error)
						.to.be.instanceof(StateMachineError)
						.and.have.property(
							"code",
							StateMachineError.ERROR_CODE.DUPLICATED_STATE
						);
				}
			});

			it(`"constructor" throw StateMachineError#DUPLICATED_TRANSITION`, () => {
				try {
					new StateMachine(
						STATES.SOLID,
						{
							states: [
								{ name: STATES.SOLID, data: {} },
								{ name: STATES.LIQUID, data: {} }
							]
						},
						{
							transitions: [
								{
									name: TRANSITIONS.MELT, // DUPLICATED_TRANSITION
									from: STATES.SOLID,
									to: STATES.LIQUID
								},
								{
									name: TRANSITIONS.MELT, // DUPLICATED_TRANSITION
									from: STATES.SOLID,
									to: STATES.LIQUID
								}
							]
						}
					);
				} catch (error) {
					expect(error)
						.to.be.instanceof(StateMachineError)
						.and.have.property(
							"code",
							StateMachineError.ERROR_CODE.DUPLICATED_TRANSITION
						);
				}
			});

			it(`"constructor" throw StateMachineError#ABSENT_STATE (to)`, () => {
				try {
					new StateMachine(
						STATES.SOLID,
						{
							states: [
								{ name: STATES.SOLID, data: {} },
								{ name: STATES.LIQUID, data: {} }
							]
						},
						{
							transitions: [
								{
									name: TRANSITIONS.MELT,
									from: STATES.SOLID,
									to: STATES.GAS // ABSENT_STATE to
								}
							]
						}
					);
				} catch (error) {
					expect(error)
						.to.be.instanceof(StateMachineError)
						.and.have.property(
							"code",
							StateMachineError.ERROR_CODE.ABSENT_STATE
						);
				}
			});

			it(`"constructor" throw StateMachineError#ABSENT_STATE (from)`, () => {
				try {
					new StateMachine(
						STATES.SOLID,
						{
							states: [
								{ name: STATES.SOLID, data: {} },
								{ name: STATES.LIQUID, data: {} }
							]
						},
						{
							transitions: [
								{
									name: TRANSITIONS.MELT,
									from: STATES.GAS, // ABSENT_STATE from
									to: STATES.LIQUID
								}
							]
						}
					);
				} catch (error) {
					expect(error)
						.to.be.instanceof(StateMachineError)
						.and.have.property(
							"code",
							StateMachineError.ERROR_CODE.ABSENT_STATE
						);
				}
			});
		});

		describe(`"doTransition" throw StateMachineError`, () => {
			it(`"doTransition" throw StateMachineError#${
				StateMachineError.ERROR_CODE.PENDING_STATE
			}`, async () => {
				await TestSM.doTransition(TRANSITIONS.MELT);
				expect(TestSM.isPending).is.false;
				TestSM.doTransition(TRANSITIONS.VAPORIZE);
				expect(TestSM.isPending).is.true;

				try {
					await TestSM.doTransition(TRANSITIONS.MELT);
				} catch (error) {
					expect(error)
						.to.be.instanceOf(StateMachineError)
						.and.have.property(
							"code",
							StateMachineError.ERROR_CODE.PENDING_STATE
						);
				}
			});

			it(`"doTransition" throw StateMachineError#${
				StateMachineError.ERROR_CODE.ABSENT_TRANSITION
			}`, async () => {
				try {
					await TestSM.doTransition(TRANSITIONS.SUBLIMATION);
				} catch (error) {
					expect(error)
						.to.be.instanceOf(StateMachineError)
						.and.have.property(
							"code",
							StateMachineError.ERROR_CODE.ABSENT_TRANSITION
						);
				}
			});

			it(`"doTransition" throw StateMachineError#${
				StateMachineError.ERROR_CODE.UNAVAILABLE_TRANSITION
			}`, async () => {
				try {
					await TestSM.doTransition(TRANSITIONS.VAPORIZE);
				} catch (error) {
					expect(error)
						.to.be.instanceOf(StateMachineError)
						.and.have.property(
							"code",
							StateMachineError.ERROR_CODE.UNAVAILABLE_TRANSITION
						);
				}
			});
		});

		describe(`"transitTo" throw StateMachineError`, () => {
			it(`"transitTo" throw StateMachineError#${
				StateMachineError.ERROR_CODE.PENDING_STATE
			}`, async () => {
				await TestSM.transitTo(STATES.LIQUID);
				expect(TestSM.isPending).is.false;
				TestSM.transitTo(STATES.GAS);
				expect(TestSM.isPending).is.true;

				try {
					await TestSM.transitTo(STATES.LIQUID);
				} catch (error) {
					expect(error)
						.to.be.instanceOf(StateMachineError)
						.and.have.property(
							"code",
							StateMachineError.ERROR_CODE.PENDING_STATE
						);
				}
			});

			it(`"transitTo" throw StateMachineError#${
				StateMachineError.ERROR_CODE.ABSENT_STATE
			}`, async () => {
				try {
					await TestSM.transitTo(STATES.PLASMA);
				} catch (error) {
					expect(error)
						.to.be.instanceOf(StateMachineError)
						.and.have.property(
							"code",
							StateMachineError.ERROR_CODE.ABSENT_STATE
						);
				}
			});

			it(`"transitTo" throw StateMachineError#${
				StateMachineError.ERROR_CODE.UNAVAILABLE_STATE
			}`, async () => {
				try {
					await TestSM.transitTo(STATES.SOLID);
				} catch (error) {
					expect(error)
						.to.be.instanceof(StateMachineError)
						.and.have.property(
							"code",
							StateMachineError.ERROR_CODE.UNAVAILABLE_STATE
						);
				}
			});
		});

		it(`"is" throw StateMachineError#PENDING_STATE`, async () => {
			const testPromise = new Promise<STATES>(resolve => {
				setTimeout(() => {
					resolve(STATES.LIQUID);
				}, 0);
			});
			await TestSM.is(testPromise);
			TestSM.is(() => STATES.LIQUID);
			TestSM.is(STATES.LIQUID);
			expect(TestSM.isPending).is.false;
			TestSM.is(testPromise);
			expect(TestSM.isPending).is.true;

			try {
				await TestSM.is(Promise.resolve(STATES.SOLID));
			} catch (error) {
				expect(error)
					.to.be.instanceOf(StateMachineError)
					.and.have.property(
						"code",
						StateMachineError.ERROR_CODE.PENDING_STATE
					);
			}
		});

		it(`"canTransitTo" throw StateMachineError#PENDING_STATE`, async () => {
			const testPromise = new Promise<STATES>(resolve => {
				setTimeout(() => {
					resolve(STATES.LIQUID);
				}, 0);
			});
			await TestSM.canTransitTo(testPromise);
			TestSM.canTransitTo(() => STATES.LIQUID);
			TestSM.canTransitTo(STATES.LIQUID);
			expect(TestSM.isPending).is.false;
			TestSM.canTransitTo(testPromise);
			expect(TestSM.isPending).is.true;

			try {
				await TestSM.canTransitTo(Promise.resolve(STATES.SOLID));
			} catch (error) {
				expect(error)
					.to.be.instanceOf(StateMachineError)
					.and.have.property(
						"code",
						StateMachineError.ERROR_CODE.PENDING_STATE
					);
			}
		});

		it(`"canDoTransition" throw StateMachineError#PENDING_STATE`, async () => {
			const testPromise = new Promise<TRANSITIONS>(resolve => {
				setTimeout(() => {
					resolve(TRANSITIONS.CONDENSE);
				}, 0);
			});
			await TestSM.canDoTransition(testPromise);
			TestSM.canDoTransition(() => TRANSITIONS.CONDENSE);
			TestSM.canDoTransition(TRANSITIONS.CONDENSE);
			expect(TestSM.isPending).is.false;
			TestSM.canDoTransition(testPromise);
			expect(TestSM.isPending).is.true;

			try {
				await TestSM.canDoTransition(
					Promise.resolve(TRANSITIONS.CONDENSE)
				);
			} catch (error) {
				expect(error)
					.to.be.instanceOf(StateMachineError)
					.and.have.property(
						"code",
						StateMachineError.ERROR_CODE.PENDING_STATE
					);
			}
		});
	});
});
