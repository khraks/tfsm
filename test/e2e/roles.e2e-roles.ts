import {Role, Selector} from "testcafe";

export const UserWhichSeeCookiePopup = Role(
	"https://newtonclub.ru/Registration",
	async t => {
		await t
			.click(Selector("[data-js='popup-close popup-close-guest']", {
				visibilityCheck: true
			}));
	});
