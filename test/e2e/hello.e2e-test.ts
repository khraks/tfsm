import {Selector} from "testcafe";
import {UserWhichSeeCookiePopup} from "./roles.e2e-roles";

fixture("Newtonclub Test")
	.page("https://newtonclub.ru/");
// fixture`Hello Test`.page("http://www.deleteagency.com/");

test.skip("Make enquiry", t => {
	return t
		.click("[data-tst-btn-guest!ERROR!]")
		.takeScreenshot();
});

test("Проверка на робота", async t => {
	await t
		.useRole(UserWhichSeeCookiePopup)
		.click("[data-tst-btn-registration]")
		.typeText("[data-tst-owner-lastname]", "Пивовар Иван Таранов")
		.typeText("[data-tst-owner-reg-code]", "Встал сегодня очень рано")
		.click("[data-tst-btn-reg-next]");
	await t
		.expect(await Selector("[data-tst-error]").visible)
		.eql(true, "Сообщение о том, что юзер не прошел проверку на робота, не появилось!")
});

test("Проверка на робота №2", async t => {
	await t
		.useRole(UserWhichSeeCookiePopup)
		.click("[data-tst-btn-registration]")
		.typeText("[data-tst-owner-lastname]", "Пивовар Иван Таранов")
		.typeText("[data-tst-owner-reg-code]", "Встал сегодня очень рано")
		.click("[data-tst-btn-reg-next]");

	await t
		.expect(await Selector("[data-tst-error]").visible)
		.eql(true, "Сообщение о том, что юзер не прошел проверку на робота, не появилось!")
});
