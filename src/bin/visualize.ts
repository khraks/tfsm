import {StateMachine, STATES, TRANSITIONS} from "../StateMachine";

export default function visualize(visualizedStateMachineClass: StateMachine<any, any, any>): string {
	let result = "graph TD \n";
	let nodeCache: { [key: string]: number } = {};
	let nodeIndex = 1;

	function getNodeIdFromCache(nodeName: string): number {
		return nodeCache[nodeName] || (nodeCache[nodeName] = nodeIndex++);
	}

	visualizedStateMachineClass[TRANSITIONS]
		.map(transition => {
			const startNodeName = transition.from.toString();
			const finishNodeName = transition.to.toString();

			const startNodeState = visualizedStateMachineClass[STATES].find(state => state.name === startNodeName);
			const finishNodeState = visualizedStateMachineClass[STATES].find(state => state.name === finishNodeName);

			const startNodeId = getNodeIdFromCache(startNodeName);
			const finishNodeId = getNodeIdFromCache(transition.to);

			result += `    ${startNodeId}(${(<any>startNodeState).before.length} ${startNodeName} ${(<any>startNodeState).after.length}) --> |${transition.before.length} ${transition.name} ${transition.after.length}| ${finishNodeId}(${(<any>finishNodeState).before.length} ${finishNodeName} ${(<any>finishNodeState).after.length})\n`;
		});

	return result;
}
