import {StateMachine} from "../src/StateMachine";

export enum STATES {
	SOLID = "SOLID",
	LIQUID = "LIQUID",
	GAS = "GAS",
	PLASMA = "PLASMA",
	GOLD = "GOLD"
}

export enum TRANSITIONS {
	MELT = "MELT",
	"FAST_MELTING" = "FAST_MELTING",
	VAPORIZE = "VAPORIZE",
	CONDENSE = "CONDENSE",
	FREEZE = "FREEZE",
	SUBLIMATION = "SUBLIMATION",
	DESUBLIMATION = "DESUBLIMATION",
	RECOMBINATION = "RECOMBINATION",
	IONIZATION = "IONIZATION",
	"PHILOSOFERS_STONE" = "PHILOSOFERS_STONE",
}

const noop = () => {
};

export default new StateMachine<STATES, TRANSITIONS, number>(
	STATES.SOLID,
	{
		states: [{
			name: STATES.SOLID,
			data: -100
		}, {
			name: STATES.LIQUID,
			data: 50
		}, {
			name: STATES.GAS,
			data: 200
		}, {
			before: [noop, noop, noop],
			name: STATES.PLASMA,
			data: 5000,
			after: [noop, noop],
		}]
	}, {
		transitions: [{
			before: [noop],
			name: TRANSITIONS.MELT,
			from: STATES.SOLID,
			to: STATES.LIQUID,
			after: [noop, noop, noop],
		}, {
			before: [noop, noop, noop],
			name: TRANSITIONS.FREEZE,
			from: STATES.LIQUID,
			to: STATES.SOLID,
		}, {
			name: TRANSITIONS.VAPORIZE,
			from: STATES.LIQUID,
			to: STATES.GAS,
		}, {
			name: TRANSITIONS.CONDENSE,
			from: STATES.GAS,
			to: STATES.LIQUID,
		}, {
			name: TRANSITIONS.SUBLIMATION,
			from: STATES.SOLID,
			to: STATES.GAS,
		}, {
			name: TRANSITIONS.DESUBLIMATION,
			from: STATES.GAS,
			to: STATES.SOLID,
		}, {
			name: TRANSITIONS.RECOMBINATION,
			from: STATES.PLASMA,
			to: STATES.GAS,
		}, {
			name: TRANSITIONS.IONIZATION,
			from: STATES.GAS,
			to: STATES.PLASMA,
		}]
	});
