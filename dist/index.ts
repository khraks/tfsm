export {
	StateMachine,
	ITransition,
	ICancelableHook,
	StateMachineError
} from "../src/StateMachine";
