import {Config} from "karma";

module.exports = function (config) {
	config.set({
		frameworks: [
			"mocha",
			"karma-typescript"
		],
		browsers: ["Chrome", "Firefox"],
		files: [
			"src/*.ts",
			"test/*.ts"
		],
		preprocessors: {
			"src/**/*.ts": ["karma-typescript", "coverage"],
			"test/**/*.ts": ["karma-typescript"]
		},
		reporters: [
			// "nyan",
			"mocha",
			"coverage",
			"summary"
		],
		client: {
			mocha: {
				reporter: "html",
				opts: true
			}
		},
		karmaTypescriptConfig: {
			tsconfig: "./tsconfig.json"
		},
		coverageReporter: {
			reporters: [
				{type: "html", dir: "coverage/"},
				{type: "text"}
			],
		},
		mochaReporter: {
			showDiff: true,
			output: "autowatch"
		},
		summaryReporter: {
			show: "all",
			// Show an 'all' column as a summary
			overviewColumn: true
		}
	});
};
